'use strict'
const { expect } = require('chai')
const { describe, it } = require('mocha')
const BlockChain = require('./blockchain')

describe('Build block data structure', () => {
  let blockchain
  beforeEach(async () => {
    blockchain = await new BlockChain()
  })
  describe('GenesisBlock', () => {
    it('should create genesis block', async () => {
      expect(blockchain.chain[0]).property(
        'nonce' ||
          'hash' ||
          'index' ||
          'previousHash' ||
          'timestamp' ||
          'transactions'
      )
    })
  })
  // describe('CreateNewBlock', () => {
  //     it('should create new block', async () => {

  //     })
  // })
})
