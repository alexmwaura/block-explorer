/* eslint-disable no-loops/no-loops */
/* eslint-disable this/no-this */
'use strict'
const { v1: uuid } = require('uuid')
const sha256 = require('sha256')
const currentNodeUrl = process.argv[3]

/**  Constructor function
 * @public
 */
function Blockchain () {
  this.chain = []
  this.pendingTransactions = []

  this.currentNodeUrl = currentNodeUrl
  this.networkNodes = []

  this.createNewBlock(100, '0', '0')
}

/**
 * Create a new block.
 * @param {number} nonce - proof of work.
 * @param {string} hash - hash value of a block.
 * @param {string} previousHash - previous block hash value.
 */

Blockchain.prototype.createNewBlock = function (nonce, previousHash, hash) {
  const newBlock = {
    index: this.chain.length + 1,
    timestamp: Date.now().toString(),
    transactions: this.pendingTransactions,
    nonce: nonce,
    hash: hash,
    previousHash: previousHash
  }
  this.pendingTransactions = []
  this.chain.push(newBlock)
  return newBlock
}

/**
 * Get last block created.
 */

Blockchain.prototype.getLastBlock = function () {
  return this.chain[this.chain.length - 1]
}

/**
 * Create a new transaction.
 * @param {string} any
 * Returns last chain
 * @function getLastBlock()
 */

Blockchain.prototype.createNewTransactions = function (args) {
  const newTransaction = {
    ...args,
    transactionId: uuid().split('-').join('')
  }
  return newTransaction
}

Blockchain.prototype.addTransactionToPendingTransactions = function (transactionObj) {
  this.pendingTransactions.push(transactionObj)
  return this.getLastBlock().index + 1
}

/**
 * Cryptography / Hash Function
 * @requires sha256
 * @param {string} previousHash
 * @param {object} currentBlockData
 * @param {number} nonce
 */

Blockchain.prototype.hashBlock = function (
  previousHash,
  currentBlockData,
  nonce
) {
  return sha256(
    `${previousHash + nonce.toString() + JSON.stringify(currentBlockData)}`
  )
}

/**
 * Proof of Work Method
 * @param previousHash
 * @param currentBlockData
 */

Blockchain.prototype.proofOfWork = function (
  previousHash,
  currentBlockData,
  nonce = 0
) {
  let hash = this.hashBlock(previousHash, currentBlockData, nonce)
  while (hash.substring(0, 4) !== '0000') {
    nonce++
    hash = this.hashBlock(previousHash, currentBlockData, nonce)
  }
  return nonce
}

Blockchain.prototype.chainIsValid = function (blockchain) {
  let validChain = true

  for (var i = 1; i < blockchain.length; i++) {
    const currentBlock = blockchain[i]
    const prevBlock = blockchain[i - 1]
    const blockHash = this.hashBlock(
      prevBlock.hash,
      {
        transactions: currentBlock.transactions,
        index: currentBlock.index
      },
      currentBlock.nonce
    )
    if (blockHash.substring(0, 4) !== '0000') validChain = false
    if (currentBlock.previousHash !== prevBlock.hash) { validChain = false }
  }

  const genesisBlock = blockchain[0]
  const correctNonce = genesisBlock.nonce === 100
  const correctPreviousBlockHash = genesisBlock.previousHash === '0'
  const correctHash = genesisBlock.hash === '0'
  const correctTransactions = genesisBlock.transactions.length === 0

  if (
    !correctNonce ||
    !correctPreviousBlockHash ||
    !correctHash ||
    !correctTransactions
  ) { validChain = false }

  return validChain
}

module.exports = Blockchain
