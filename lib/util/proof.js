'use strict'

module.exports = (bitcoin) => {
  const lastBlock = bitcoin.getLastBlock()
  const prevHash = lastBlock.hash
  const currentBlock = {
    transactions: bitcoin.pendingTransactions,
    index: lastBlock.index + 1
  }
  const nonce = bitcoin.proofOfWork(prevHash, currentBlock)
  const hash = bitcoin.hashBlock(
    prevHash,
    currentBlock,
    nonce
  )
  return {
    nonce,
    prevHash,
    hash
  }
}
