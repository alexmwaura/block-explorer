/* eslint-disable eqeqeq */
/* eslint-disable no-empty-pattern */
'use strict'

const Query = require('./query')
const Mutation = require('./mutation')

exports.resolvers = {
  Query: Query,
  Mutation: Mutation
}
