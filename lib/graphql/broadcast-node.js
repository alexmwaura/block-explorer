'use strict'
const rp = require('request-promise')

module.exports = (bitcoin, newNodeUrl) => {
  const registerNodesPromises = []
  bitcoin.networkNodes.forEach((networkNodeUrl) => {
    const requestOptions = {
      uri: `${networkNodeUrl}/graphql`,
      method: 'POST',
      body: {
        query:
            `mutation RegisterNode($nodeUrl: String!) {
                registerNode(nodeUrl: $nodeUrl) {
                  response
                }
              }`,
        variables: {
          nodeUrl: newNodeUrl
        }
      },
      json: true
    }
    const results = rp(requestOptions)
    registerNodesPromises.push(results)
  })
  return Promise.all(registerNodesPromises)
    .then(() => {
      const bulkRegisterOptions = {
        uri: `${newNodeUrl}/graphql`,
        method: 'POST',
        body: {
          query: `mutation RegisterNodesBulk($allNetworkNodes: [String!]!) {
                registerNodesBulk(allNetworkNodes: $allNetworkNodes) {
                  response
                }
              }`,
          variables: {
            allNetworkNodes: [
              ...bitcoin.networkNodes,
              bitcoin.currentNodeUrl
            ]
          }
        },
        json: true
      }
      return rp(bulkRegisterOptions)
    })
    .then(() => {
      return {
        response: 'New node registered with network successfully'
      }
    })
}
