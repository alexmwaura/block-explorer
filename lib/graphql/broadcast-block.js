'use strict'
const rp = require('request-promise')

module.exports = (bitcoin, newBlock, nodeAddress) => {
  const requestPromises = []
  bitcoin.networkNodes.forEach((networkNodeUrl) => {
    const requestOptions = {
      uri: `${networkNodeUrl}/graphql`,
      method: 'POST',
      body: {
        query:
          `mutation ReceiveNewBlock($newBlock: BlockInput!){
            receiveNewBlock(newBlock: $newBlock) {
              response
            }
          }  
          `,
        variables: {
          newBlock: {
            ...newBlock
          }
        }
      },
      json: true
    }
    requestPromises.push(rp(requestOptions))
  })
  return Promise.all(requestPromises)
    .then(() => {
      const requestOptions = {
        uri: `${bitcoin.currentNodeUrl}/graphql`,
        method: 'POST',
        body: {
          query: `
            mutation TransactionBroadCast($amount: Float!, $sender: String!, $recepient: String!) {
                transactionBroadCast(amount: $amount, sender: $sender, recepient: $recepient) {
                  response
                }
              }
            `,
          variables: {
            amount: 12.5,
            sender: '00',
            recepient: nodeAddress
          }
        },

        json: true
      }

      return rp(requestOptions)
    })
    .then(() => {
      return {
        response: 'New block mined & broadcast successfully',
        block: newBlock
      }
    })
}
