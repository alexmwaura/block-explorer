'use strict'
const rp = require('request-promise')

module.exports = (bitcoin) => {
  const requestPromises = []
  bitcoin.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: `${networkNodeUrl}/graphql`,
      method: 'POST',
      body: {
        query: `query GetAllBitcoins {
          getAllBitcoins {
            chain {
              index
              timestamp
              transactions {
                amount
                sender
                recepient
                transactionId
              }
              nonce
              hash
              previousHash
            }
            currentNodeUrl
            networkNodes
            pendingTransactions {
              amount
              sender
              recepient
              transactionId
            }
          }
        }`
      },
      json: true
    }

    requestPromises.push(rp(requestOptions))
  })

  return Promise.all(requestPromises)
    .then(blockchains => {
      const currentChainLength = bitcoin.chain.length
      let maxChainLength = currentChainLength
      let newLongestChain = null
      let newPendingTransactions = null

      blockchains.forEach(blockchain => {
        const chainLength = blockchain.data.getAllBitcoins.chain.length
        const pendingTransactions = blockchain.data.getAllBitcoins.pendingTransactions
        if (chainLength > maxChainLength) {
          maxChainLength = chainLength
          newLongestChain = blockchain.data.getAllBitcoins.chain
          newPendingTransactions = pendingTransactions
        };
      })

      if (!newLongestChain || (newLongestChain && !bitcoin.chainIsValid(newLongestChain))) {
        return {
          response: 'Current chain has not been replaced.',
          chain: bitcoin.chain
        }
      } else {
        bitcoin.chain = newLongestChain
        bitcoin.pendingTransactions = newPendingTransactions
        return {
          response: 'This chain has been replaced.',
          chain: bitcoin.chain
        }
      }
    })
}
