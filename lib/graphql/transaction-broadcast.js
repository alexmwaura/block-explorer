'use strict'
const rp = require('request-promise')

module.exports = (transactionObj, bitcoin) => {
  const newTransaction = bitcoin.createNewTransactions(transactionObj)
  bitcoin.addTransactionToPendingTransactions(newTransaction)
  const requestPromises = []
  bitcoin.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: `${networkNodeUrl}/graphql`,
      method: 'POST',
      body: {
        query:
          `mutation CreateTransaction($transaction: TransactionInput!) {
            createTransaction(transaction: $transaction) {
              response
            }
          }
          `,
        variables:
        {
          transaction: {
            ...newTransaction
          }
        }
      },
      json: true
    }
    requestPromises.push(rp(requestOptions))
  })

  return Promise.all(requestPromises)
    .then(() => {
      return ({ response: 'Transaction created and broadcast successfully.' })
    })
}
