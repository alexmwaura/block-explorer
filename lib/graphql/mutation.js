/* eslint-disable eqeqeq */
/* eslint-disable no-empty-pattern */
'use strict'
const broadCastNode = require('./broadcast-node')
const transactionBroadCast = require('./transaction-broadcast')

module.exports = {
  createTransaction: (
    _,
    args,
    { dataSources: { bitcoin } }
  ) => {
    const blockIndex = bitcoin.addTransactionToPendingTransactions(args.transaction)
    return { response: `Transaction will be added in block: ${blockIndex}` }
  },

  // receive new block
  receiveNewBlock: (_, { newBlock }, { dataSources: { bitcoin } }) => {
    const lastBlock = bitcoin.getLastBlock()
    const correctHash = lastBlock.hash === newBlock.previousHash
    const correctIndex = lastBlock.index + 1 === newBlock.index
    if (correctHash && correctIndex) {
      bitcoin.chain.push(newBlock)
      bitcoin.pendingTransactions = []
      return {
        response: 'New block received and accepted',
        newBlock: newBlock
      }
    } else throw new Error('New block rejected')
  },
  // register a node and broadcast it to the network
  registerBroadCastNode: (_, { newNodeUrl }, { dataSources: { bitcoin } }) => {
    if (newNodeUrl == bitcoin.currentNodeUrl) throw new Error('Cannot broadcast to self')
    if (bitcoin.networkNodes.indexOf(newNodeUrl) == -1) bitcoin.networkNodes.push(newNodeUrl)
    else throw new Error('Node uri already exists')
    return broadCastNode(bitcoin, newNodeUrl)
  },

  // register a node with the network
  registerNode: (_, { nodeUrl }, { dataSources: { bitcoin } }) => {
    const notCurrentNode = bitcoin.currentNodeUrl !== nodeUrl
    const nodeNotAlreadyPresent = bitcoin.networkNodes.indexOf(nodeUrl) == -1
    if (nodeNotAlreadyPresent && notCurrentNode) bitcoin.networkNodes.push(nodeUrl)
    else throw new Error('Cannot broadcast to self')
    return {
      response: 'New node registered successfully with node'
    }
  },

  // register multiple nodes at once
  registerNodesBulk: (_, { allNetworkNodes }, { dataSources: { bitcoin } }) => {
    allNetworkNodes.forEach((networkNode) => {
      const nodeNotAlreadyPresent = bitcoin.networkNodes.indexOf(networkNode) == -1
      const notCurrentNode = bitcoin.currentNodeUrl !== networkNode
      if (nodeNotAlreadyPresent && notCurrentNode) bitcoin.networkNodes.push(networkNode)
    })
    return {
      response: 'Bulk registration successful.'
    }
  },

  // broadcast transaction data
  transactionBroadCast: (_, args, { dataSources: { bitcoin } }) => {
    return transactionBroadCast(args, bitcoin)
    // return null
  }
}
