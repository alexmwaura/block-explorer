/* eslint-disable no-empty-pattern */
'use strict'
const { v1: uuid } = require('uuid')
const proofOfWork = require('../util/proof')
const broadCastBlock = require('./broadcast-block')
const nodeAddress = uuid().split('-').join('')
const blockConsensus = require('./block-consensus')

module.exports = {
  getAllBitcoins: async (_, {}, { dataSources: { bitcoin } }) => {
    return bitcoin
  },
  mineBitCoin: async (_, {}, { dataSources: { bitcoin } }) => {
    const { nonce, prevHash, hash } = proofOfWork(bitcoin)
    bitcoin.createNewTransactions(12.5, '00', nodeAddress)
    const newBlock = bitcoin.createNewBlock(nonce, prevHash, hash)
    return broadCastBlock(bitcoin, newBlock, nodeAddress)
  },
  getConsensus: (_, {}, { dataSources: { bitcoin } }) => {
    return blockConsensus(bitcoin)
  }

}
