/* eslint-disable security/detect-non-literal-fs-filename */
'use strict'
// Apollo server imports
const { ApolloServer } = require('apollo-server')
const { ApolloServerPluginCacheControl } = require('apollo-server-core')
const fs = require('fs')
const path = require('path')

const Blockchain = require('../lib/blockchain/blockchain')

const { resolvers } = require('../lib/graphql/graphql-resolvers')

// model
const Bitcoin = new Blockchain()
const port = process.argv[2]

// setup server
const server = new ApolloServer({
  typeDefs: fs.readFileSync(
    path.join(__dirname, 'schema.graphql'),
    'utf8'
  ),
  resolvers,
  dataSources: () => {
    return {
      bitcoin: Bitcoin
    }
  },

  plugins: [ApolloServerPluginCacheControl({ defaultMaxAge: 500 })]

})
server.listen(port).then(({ url }) => {
  console.log(`server running on: ${url}`)
})
