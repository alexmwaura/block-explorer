'use strict'

const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const Blockchain = require('../lib/blockchain/blockchain')

const bitcoin = new Blockchain()

// welcome route

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => {
  return res.json({
    welcome: 'Welcome to my blockchain api server'
  })
})

app.get('/blockchain', (req, res) => {
  return res.json(bitcoin)
})

// create new transaction
app.post('/transaction', async (req, res) => {
  const blockIndex = bitcoin.createNewTransactions(req.body.amount, req.body.sender, req.body.recepient)
  return res.json({ note: `Transaction will be added in block ${blockIndex}.` })
})

app.listen(3000, () => {
  console.log('Now listening for request on port 3000')
})
