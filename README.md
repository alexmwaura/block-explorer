`author:` Alex Mwaura

# Simple Blockchain

### Introduction
The initial objective of the blockchain technology was to establish trusted financial transactions between two independent parties without any involvement of third-parties such as a bank; however, later, several industries adopted blockchain to streamline their supply chain process, know your customer system (KYC), data management, and so on.

With the growing use of online services and a growing number of online transactions, users have to trust and depend on third parties such as banks and payment gateway providers. This led to the birth of the blockchain.

***Requirements of any useful blockchain***
- Detect who looks at a person's account and changes it
- Ensure that data concerning the person is not misused

This sounds like a smart thing to use, but it's not that difficult to implement. This is what a blockchain does. In a nutshell, it's nothing but a smart, safe, and constantly growing database.

Blockchain is a chronological ledger that records transactions of any value or asset securely. 

- ***It is an immutable, distributed Ledger***

The blockchain network provides the ability to transfer any type of value or asset between independent parties using a peer-to-peer network.


### Language
JavaScript

### Dependencies

### Tasks
- ### Build blockchain Data Structure
    - [x] Proof of work
    - [x] Mine new blocks
    - [x] Create transactions
    - [x] Validate the chain
    - [x] Retrieve address data
    - [ ] Other methods `'to be updated'`
    
- ###  Build blockchain API/Server
    - [ ] Allow access of the block via a public server
    - [ ] Enable interaction of the blockchain through methods / functionality defined in the database structure

- ###  Build decentralized blockchain network 
    - [ ] Multiple Servers running
    - [ ] Servers act as separate nodes
    - [ ] Nodes are able to interact with each other 
    - [ ] Nodes are able to share data in the correct format
    - [ ] Synchronize network to broadcast transactions or blocks

- ### Build Network consensus algorithm
    - [ ] Make sure entire network stays synchronized 
    - [ ] Each node has correct blockchain data

- ### Connect to Public Server
```
 Still researching
    ideas: Ethereum, HyperLedger
    reasoning: One node to connect the rest of the nodes
 ```

- ### Frontend
    
    ---
    `Working on the rest` 


### Sources:
- [ Rajneesh Gupta. “Hands-On Cybersecurity with Blockchain.”](https://www.packtpub.com/product/hands-on-cybersecurity-with-blockchain/9781788990189)

