### Tasks
- ### Build blockchain Data Structure
    - [x] Proof of work
    - [x] Mine new blocks
    - [x] Create transactions
    - [x] Validate the chain
    - [x] Retrieve address data
    - [ ] Other methods `'to be updated'`
    
- ###  Build blockchain API/Server
    - [ ] Allow access of the block via a public server
    - [ ] Enable interaction of the blockchain through methods / functionality defined in the database structure

- ###  Build decentralized blockchain network 
    - [ ] Multiple Servers running
    - [ ] Servers act as separate nodes
    - [ ] Nodes are able to interact with each other 
    - [ ] Nodes are able to share data in the correct format
    - [ ] Synchronize network to broadcast transactions or blocks

- ### Build Network consensus algorithm
    - [ ] Make sure entire network stays synchronized 
    - [ ] Each node has correct blockchain data
